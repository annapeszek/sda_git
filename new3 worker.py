from abc import ABC, abstractmethod


class Worker(ABC):
    @abstractmethod
    def calculateSalary(self):
        pass

    @staticmethod
    def compareSalary(worker1, worker2):
        if worker1.calculateSalary() > worker2.calculateSalary():
            return 1  # worker1.__str__()
        else:
            return 2  # worker2.__name__


class RegularWorker(Worker):
    def __init__(self, monthSalary):
        self.monthSalary = monthSalary

    def calculateSalary(self):
        return self.monthSalary


class HourWorker(Worker):
    def __init__(self, hourSalary, time):
        self.hourSalary = hourSalary
        self.time = time

    def calculateSalary(self):
        return self.hourSalary * self.time


class Team():
    def __init__(self, listOfWorkers):
        self.listOfWorkers = listOfWorkers

    def calculateTeamSalary(self):
        salary = 0
        for worker in self.listOfWorkers:
            salary += worker.calculateSalary()
        return salary


if __name__ == '__main__':
    h = HourWorker(40, 168)
    r = RegularWorker(2000)
    x = RegularWorker(1500)
    print(h.calculateSalary())
    print(r.calculateSalary())
    print(Worker.compareSalary(h, r))
    t = Team([h, r, x])
    print(t.calculateTeamSalary())
