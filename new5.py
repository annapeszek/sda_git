

import random
import string


class RoadSign:
    def __init__(self, speedLimit):
        self.speedLimit = speedLimit
        text = ''.join(random.choice(string.ascii_letters) for i in range(5))
        self.ID = str(random.randrange(1000, 9000)) + text

    def __str__(self):
        return f'Sign {self.ID}: speed limit {self.speedLimit}'


class Signs:
    def __init__(self):
        self.roadSignList = []
        for i in range(10):
            self.roadSignList.append(RoadSign(100 - i * 10))

    def __repr__(self):
        for sign in self.roadSignList:
            print(sign)
        return ''
    # def __repr__(self):
    #     return (sign.__str__() for sign in self.roadSignList)


    def __getitem__(self, item):
        return self.roadSignList[item]

    def __len__(self):
        return len(self.roadSignList)


if __name__ == '__main__':
    signs = Signs()
    print(signs[2])
    print('List of Signs:')
    print(signs)
    print('number of signs:')
    print(len(signs))