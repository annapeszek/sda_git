class A:
    """class documentation"""

    def go(self):
        """method documentation"""
        print('go')


a = A()
print(a.__doc__)
print(a.go.__doc__)
