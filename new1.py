from abc import ABC, abstractmethod


class Vehicle(ABC):

    @abstractmethod
    def go(self):
        pass

    @abstractmethod
    def stop(self):
        pass


class Car(Vehicle):
    def __init__(self):
        self.speed = 0
        self.turnMotor = False

    def go(self):
        if not self.turnMotor:
            self.turnOnMotor()
        self.speed += 2
        print(f'Car speed {self.speed}')

    def turnOnMotor(self):
        self.motor = True

    def stop(self):
        self.speed = 0
        self.motor = False


class Bicycle(Vehicle):
    def __init__(self):
        self.speed = 0

    def go(self):
        self.speed += 1
        print('Bicycle speed', self.speed)

    def stop(self):
        self.speed = 0


if __name__ == '__main__':
    c = Car()
    b = Bicycle()
    c.go()
    c.go()
    b.go()
    # c.stop()
    # print(c.speed)
    c.__class__=Vehicle #zmienia klasę obiektu
    c.go()
    print(c.speed)
